# README #

Simple modal widget.

#Overview
Simple modal which can be used for collecting users emails.

#Usage
Add styles and scripts files to your document

```
#!html
<link rel="stylesheet" href="widget/modal.css">
<script src="widget/modal.js"></script>
```

Then you can create new modal instance:
```
#!javascript
    var myModal = new modal.init({
      header: 'Modal header',
      message: 'Your message',
      image: 'path/to/img',
      close: function(){ /* Function wich will be invoked on modal close */},
      clickOk: function(){ /* Function which will trigger on modal confirm */ },
      theme: 'light'
    });
```
### Trigger modal
You can show modal by adding onclick to element:

```
#!html
<a href="#" onclick="myModal.show()">Show!</a>
```

or in your js file
```
#!javascript
myModal.show();
```
### Get data from it
If user typed correct email you can grab data by creating event listener. Modal passing user email to your callback function.
```
#!javascript
    document.addEventListener('modalConfirmed', function(data) {
       alert(data)
    });
```

Second options is to grab data in your modal declaration with your 'clickOk' function, for example:

```
#!javascript
var myModal = new modal.init({
  clickOk: function(data){alert(data)},
});
```

#Options
## modal.header (optional)
Default: ''

Modal header ;)

## modal.message (optional)
Default: ''

Modal content.

## modal.image (optional)
Default: ''

Image displayed in modal

## modal.close (optional)
Default: undefined

A function which you want to call after modal is closed by user.

## modal.clickOk (optional)
Default: undefined

A function which will be triggered after user confirmation and if email is valid.

## modal.theme (optional)
Default: 'light', possible: 'light | dark'

Modal color variant