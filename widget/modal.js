'user strict';
var modal = {
  template: '<div class="modal-container"><div class="va-middle"><div class="modal-content"><div class="modal-close"><a href="#">&times;</a></div><img class="modal-img" src="http://icons.iconarchive.com/icons/hopstarter/sleek-xp-software/256/Yahoo-Messenger-icon.png"><div class="modal-body"><div class="va-middle"><h2 class="modal-header">Lorem ipsum dolor sit.</h2><p class="modal-message">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Perspiciatis aliquam deserunt excepturi, consectetur, vero omnis similique delectus nam saepe animi!</p><input type="email"><p class="modal-error">Wrong email!</p><a href="#" class="modal-ok">Try it for free</a></div></div></div></div></div>',
  init: function(options) {
    var mod = {
        show: function() {
          mod.box.style.display = 'block';
          setTimeout(function() { mod.box.style.opacity = 1; }, 0);
        },
        close: function() {
          mod.box.style.opacity = 0;
          setTimeout(function() {
            mod.box.style.display = 'none';
            mod.box.parentNode.removeChild(modal.box);
          }, 300);
          if( mod.userClose != undefined ) {
            mod.userClose();
          }
        },
        submit: function() {
          var email = mod.box.querySelector('input');
          var reg = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
          var valid = reg.test(email.value);

          if( valid == true ) {
            var event = new CustomEvent('modalConfirmed', { 'detail': email.value });

            mod.box.querySelector('.modal-error').style.height = '0px';
            document.dispatchEvent(event);
            mod.box.style.opacity = 0;
            setTimeout(function() {
              mod.box.style.display = 'none';
              mod.box.parentNode.removeChild(mod.box);
              if( mod.userOk != undefined ) {
                mod.userOk(email.value);
              }
            }, 300);
          } else {
            mod.box.querySelector('.modal-error').style.height = '25px';
          }
        }
    };
    mod.userClose = options.close;
    mod.userOk = options.clickOk;
    mod.box = document.createElement('div');

    mod.box.className = 'modal-widget ' + (options.theme || '');
    document.body.appendChild(mod.box);
    mod.box.innerHTML = modal.template;

    mod.box.querySelector('.modal-header').innerHTML = options.header || '';
    mod.box.querySelector('.modal-message').innerHTML = options.message || '';
    mod.box.querySelector('.modal-img').src = options.image || '';

    mod.box.querySelector('.modal-close').onclick = mod.close;
    mod.box.querySelector('.modal-ok').onclick = mod.submit;

    return mod;
  },

}